#!/usr/bin/env python3
"""
Python Insights4CI' client library
Copyright (c) 2022 Beraldo Leal for Red Hat, Inc.
"""

import sys

import pkg_resources
import setuptools
from setuptools.command import bdist_egg


class BdistEggGuard(bdist_egg.bdist_egg):
    def run(self):
        sys.exit("Please use `pip install .` instead.")


def main():
    # https://medium.com/@daveshawley/safely-using-setup-cfg-for-metadata-1babbe54c108
    pkg_resources.require("setuptools>=39.2")

    kwargs = {"cmdclass": {"bdist_egg": BdistEggGuard}}

    setuptools.setup(**kwargs)


if __name__ == "__main__":
    main()
